Feature: As a JioSaavn user
  I want to log in to the web application
  So that I can use logged in user's feature

  Scenario: Login to the JioSaavn WebApp
    Given I am on the JioSaavn Home Page
    When I click on the Login Option
    And I enter email and the password
    And I click on the Log In button
    Then I land on the homepage