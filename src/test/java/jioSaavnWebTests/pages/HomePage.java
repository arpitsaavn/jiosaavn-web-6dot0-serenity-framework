package jioSaavnWebTests.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@DefaultUrl("https://www.jiosaavn.com/")
public class HomePage extends PageObject {

    public HomePage(WebDriver driver)
    {
        super(driver);
    }

    public void goToTheLoginPage()
    {
        $(By.xpath("//a[contains(text(), 'Log In')]")).click();
    }
}
