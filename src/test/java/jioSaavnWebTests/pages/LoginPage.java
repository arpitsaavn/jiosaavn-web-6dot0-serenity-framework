package jioSaavnWebTests.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class LoginPage extends PageObject {

    public void enterUserName(String emailAddress) {
        $(By.id("email")).type(emailAddress);
    }

    public void enterPassword(String password) {
        $(By.id("password")).type(password);
    }

    public void clickCaptchaCheckBox() { $(By.className("recaptcha-checkbox-border")).click(); }

    public void clickLoginButton() { $(By.xpath("//button[contains(text(), 'Continue')]")).click(); }
}
