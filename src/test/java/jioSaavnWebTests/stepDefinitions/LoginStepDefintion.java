package jioSaavnWebTests.stepDefinitions;

import java.util.concurrent.*;
import io.cucumber.java8.En;
import jioSaavnWebTests.steps.LoginSteps;
import net.thucydides.core.annotations.Steps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginStepDefintion implements En {

    Logger logger = LoggerFactory.getLogger("JioSaavn Web Test Logs");
    @Steps
    private LoginSteps loginSteps;

    public LoginStepDefintion() {
        Given("^I am on the JioSaavn Home Page$", () ->
            loginSteps.isOnTheHomePage()
        );
        When("^I click on the Login Option$", () ->
            loginSteps.openLoginPage()
        );
        And("^I enter email and the password$", () ->
            loginSteps.enterUserNameAndPassword("richipod@saavn.com", "Saavn123")

        );
        And("^I click on the Log In button$", () ->
                loginSteps.clickLoginButton()
        );
        Then("^I land on the homepage$", () ->
            logger.info("Logged In Successful")
        );
    }
}
