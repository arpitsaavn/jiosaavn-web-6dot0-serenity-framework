package jioSaavnWebTests.steps;

import jioSaavnWebTests.pages.HomePage;
import jioSaavnWebTests.pages.LoginPage;
import net.thucydides.core.annotations.Step;


public class LoginSteps {

    private HomePage homePage;
    private LoginPage loginPage;

    @Step
    public void isOnTheHomePage() {
        homePage.open();
    }

    @Step
    public void openLoginPage() {
        homePage.goToTheLoginPage();
    }

    @Step
    public void enterUserNameAndPassword (String emailAddress, String password) {
        loginPage.enterUserName(emailAddress);
        loginPage.enterPassword(password);
    }
    @Step
    public void clickLoginButton() {
        loginPage.clickLoginButton();
    }
}

